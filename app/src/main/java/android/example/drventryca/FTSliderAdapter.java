package android.example.drventryca;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.viewpager.widget.PagerAdapter;

import java.util.ArrayList;

public class FTSliderAdapter extends PagerAdapter {

    /*private ArrayList<FaktaTips> mFaktaTips;*/

    Context context;
    LayoutInflater layoutInflater;

    /*, ArrayList<FaktaTips> dataFaktaTips*/

    public FTSliderAdapter(Context context){
        this.context = context;

        faktatips_slide = context.getResources().getStringArray(R.array.fakta_tips);
        /*this.mFaktaTips =dataFaktaTips;*/
    }

    public String[] faktatips_slide;


    @Override
    public int getCount() {
        return faktatips_slide.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == (RelativeLayout) object;
    }


    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        layoutInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.slide_fakta_tips,container,false);

        TextView text_faktatips = view.findViewById(R.id.text_FaktaTips);
        TextView text_count = view.findViewById(R.id.hal_count);

        /*View page = layoutInflater.inflate(R.layout.slide_fakta_tips, null);

        page.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });*/

/*        FaktaTips current_FaktaTips = mFaktaTips.get(position);*/


        text_faktatips.setText(faktatips_slide[position]);
        int total = faktatips_slide.length;

        text_count.setText(position+1+" / "+total);
        container.addView(view);

        return view;
    }


    @Override
    public void destroyItem( ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout)object);
    }
}
