package android.example.drventryca;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class DiseaseAdapter extends RecyclerView.Adapter<DiseaseAdapter.ViewHolder> {

    private Context mContext;
    private ArrayList<DiseaseModel> diseaseModel;

    DiseaseAdapter(Context context, ArrayList<DiseaseModel> disease_list){
        this.mContext = context;
        this.diseaseModel = disease_list;
    }

    @Override
    public DiseaseAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(mContext).inflate(R.layout.disease_items,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull DiseaseAdapter.ViewHolder holder, int position) {
        DiseaseModel currentDisease = diseaseModel.get(position);
        holder.bindTo(currentDisease);
    }

    @Override
    public int getItemCount() {
        return diseaseModel.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private TextView disease_text;
        RelativeLayout  disease_card;

        public ViewHolder(View itemView) {
            super(itemView);

            disease_text = itemView.findViewById(R.id.disease_title);
            disease_card = itemView.findViewById(R.id.disease_container);

        }

        void bindTo(DiseaseModel currentDisease){
            disease_text.setText(currentDisease.getTitle_());
            disease_text.setAnimation(AnimationUtils.loadAnimation(mContext,R.anim.fade_right_show));
            disease_card.setAnimation(AnimationUtils.loadAnimation(mContext,R.anim.translate_y_show2));
        }
    }
}
