package android.example.drventryca;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

public class HomeFragment extends Fragment {

    private CardView klikDiag, klikData, klikFakta, klikLemak;
    private FaktaTipsAct faktaTipsAct;
    private ViewPager slideFaktaPager;
    private FTSliderAdapter ftSliderAdapter;
    private int mCurrentFaktaTips =3;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
         View view = inflater.inflate(R.layout.fragment_home, container, false);

         klikDiag = view.findViewById(R.id.goDiag);
         klikData = view.findViewById(R.id.goData);
         klikFakta = view.findViewById(R.id.goFakta);
         klikLemak = view.findViewById(R.id.card_lemak);

         TextView lihat_ = view.findViewById(R.id.lihat_tips_lain);

         klikDiag.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 if (v.getId() == R.id.goDiag || v.getId() == R.id.goDiag1 || v.getId() == R.id.goDiag2){
                     startActivity(new Intent(getActivity(), ReadyDiag.class));
                 }
             }
         }) ;

         klikData.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 if (v.getId() == R.id.goData || v.getId() == R.id.goData1 || v.getId() == R.id.goData2){
                     startActivity(new Intent(getActivity(), DataDisease.class));
                 }
             }
         });

         klikFakta.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 if (v.getId() == R.id.goFakta || v.getId() == R.id.goFakta1 || v.getId() == R.id.goFakta2){
                     startActivity(new Intent(getActivity(), FaktaTipsAct.class));
                 }
             }
         });

         lihat_.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 startActivity(new Intent(getActivity(), FaktaTipsAct.class));
             }
         });

         klikLemak.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 startActivity(new Intent(getActivity(), FaktaTipsAct.class));
             }
         });

         return view;
    }

}
