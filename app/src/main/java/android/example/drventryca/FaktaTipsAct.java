package android.example.drventryca;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import java.util.ArrayList;

public class FaktaTipsAct extends AppCompatActivity {

    private ViewPager mFaktaTipsPager;
    private FTSliderAdapter ftSliderAdapter;
    private ArrayList<FaktaTips> mFaktaTips;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fakta_tips);

        Window window = getWindow();

        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
                View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);

        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(Color.TRANSPARENT);

        mFaktaTipsPager = findViewById(R.id.slideFaktaTips);
        mFaktaTipsPager.setOnPageChangeListener(mListener);

        /*initializeFaktaTips();
        mFaktaTips = new ArrayList<>();*/
        ftSliderAdapter = new FTSliderAdapter(this);
        mFaktaTipsPager.setAdapter(ftSliderAdapter);
    }

    ViewPager.OnPageChangeListener mListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        }

        @Override
        public void onPageSelected(int position) {

        }

        @Override
        public void onPageScrollStateChanged(int state) {


        }
    };

/*    private void initializeFaktaTips(){
        String[] desk_faktatips = getResources().getStringArray(R.array.fakta_tips);

        mFaktaTips.clear();
        for (int i=0; i< desk_faktatips.length; i++){
            mFaktaTips.add(new FaktaTips(desk_faktatips[i]));
        }
    }*/
}
