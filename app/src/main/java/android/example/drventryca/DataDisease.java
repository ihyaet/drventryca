package android.example.drventryca;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class DataDisease extends AppCompatActivity {

    private RecyclerView diseaseRecycler_;
    private ArrayList<DiseaseModel> diseaseList;
    private DiseaseAdapter diseaseAdapter;
    private TextView title_fitur;
    private RelativeLayout card_disease;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_disease);

        title_fitur =findViewById(R.id.title_fitur);
        title_fitur.setAnimation(AnimationUtils.loadAnimation(this,R.anim.translate_y_show));


        diseaseRecycler_ = findViewById(R.id.diseaseRecycler);
        diseaseRecycler_.setLayoutManager(new LinearLayoutManager(this));
        diseaseList = new ArrayList<>();

        Window window = getWindow();

        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
                View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);

        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(Color.TRANSPARENT);

        diseaseAdapter = new DiseaseAdapter(this, diseaseList);
        diseaseRecycler_.setAdapter(diseaseAdapter);
        initializeDiseaseList();

    }

    public void initializeDiseaseList(){
        String[]title_disease =getResources().getStringArray(R.array.disease_list);

        diseaseList.clear();
        for (int i=0; i<title_disease.length;i++){
            diseaseList.add(new DiseaseModel(title_disease[i]));
        }
        diseaseAdapter.notifyDataSetChanged();
    }

}
